from app import app, db
from app.forms import LoginForm, RegisterForm, TaskForm
from app.models import User, Task
from flask import render_template, url_for, redirect, flash, request
from flask_login import login_user, logout_user, current_user, login_required

@app.route('/')
@app.route('/index', methods=["POST", "GET"])
@login_required
def index():
	form = TaskForm()
	# Check to see user data
	tasks = Task.query.filter_by(author=current_user).all()
	
	if form.validate_on_submit():
		task = Task(description=form.task.data, author=current_user)
		db.session.add(task)
		db.session.commit()
		return redirect(url_for('index'))
	return render_template("index.html", tasks=tasks, form=form)

@app.route('/login', methods=["POST", "GET"])
def login():
	form = LoginForm()
	if current_user.is_authenticated:
		return redirect(url_for('index'))
	if form.validate_on_submit():
		user = User.query.filter_by(username=form.username.data).first()
		if user is not None and user.check_password(form.password.data):
			flash('Successfully logged in as {}'.format(user.username))
			login_user(user)
		else:
			flash('Wrong username or password')
			return redirect(url_for('login'))
		return redirect(url_for('index'))
	return render_template("login.html", form=form)

@app.route('/register', methods=["POST", "GET"])
def register():
	form = RegisterForm()
	if form.validate_on_submit():
		user = User.query.filter_by(email=form.email.data).first()
		if user is None:
			user = User(username=form.username.data, email=form.email.data)
			user.set_password(form.password.data)

			db.session.add(user)
			db.session.commit()
			flash('sucessfully registered user!')
		else:
			flash('Account with that email already exists.')
			return redirect(url_for('register'))
		return redirect(url_for('index'))
	return render_template("register.html", form=form)

@app.route('/logout')
@login_required
def logout():
	logout_user()
	return redirect(url_for('login'))

@app.route('/delete', methods=["POST"])
@app.route('/delete/<int:task_id>', methods=["POST"])
def delete(task_id):
	task = Task.query.filter_by(id=task_id).first()
	db.session.delete(task)
	db.session.commit()
	return redirect(url_for('index'))
