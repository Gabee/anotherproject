from app import db, login
from datetime import datetime
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import UserMixin

@login.user_loader
def load_user(user_id):
	return User.query.get(int(user_id))

class User(db.Model, UserMixin):
	id = db.Column(db.Integer, primary_key=True)
	username = db.Column(db.String(80), unique=True, index=True)
	email = db.Column(db.String(120), unique=True, index=True)
	hash_password = db.Column(db.String(120))
	tasks = db.relationship('Task', backref="author", lazy=True)

	def set_password(self, password):
		self.hash_password = generate_password_hash(password)

	def check_password(self, password):
		return check_password_hash(self.hash_password, password)

class Task(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	description = db.Column(db.String(120))
	#timestamp = db.Column(db.DateTime, index=True, defualt=datetime.utcnow)
	user_id = db.Column(db.Integer, db.ForeignKey('user.id'))