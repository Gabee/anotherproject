import os

class Config(object):
	SECRET_KEY = os.environ.get('SECRET_KEY') or 'test'
	SQLALCHEMY_DATABASE_URI = os.environ.get('SQLALCHEMY_DATABASE_URI') or 'sqlite:///app.db'
	SQLALCHEMY_TRACK_MODIFICATIONS = False
